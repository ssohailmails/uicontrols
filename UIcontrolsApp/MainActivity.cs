﻿using System;
using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace UIcontrolsApp
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        SeekBar simpleSeekBar;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Click += FabOnClick;


            var editText = FindViewById<EditText>(Resource.Id.editTextId);
            var textView = FindViewById<TextView>(Resource.Id.TextLabelId);

            editText.TextChanged += (object sender, Android.Text.TextChangedEventArgs e) => {
                textView.Text = e.Text.ToString();
            };

            simpleSeekBar = (SeekBar)FindViewById(Resource.Id.simpleSeekBar);

            simpleSeekBar.ProgressChanged += (object sender, SeekBar.ProgressChangedEventArgs e) => {
                if (e.FromUser)
                {
                    textView.TextSize = e.Progress;
                }
            };

            CheckBox checkbox = FindViewById<CheckBox>(Resource.Id.italicCheckBoxId);

            checkbox.Click += (o, e) => {
                if (checkbox.Checked)
                    textView.SetTypeface(Typeface.SansSerif, TypefaceStyle.Italic);
                else
                {
                    textView.SetTypeface(Typeface.SansSerif, TypefaceStyle.Normal);
                }
            };

            CheckBox checkbox2 = FindViewById<CheckBox>(Resource.Id.boldCheckBoxId);

            checkbox2.Click += (o, e) => {
                if (checkbox.Checked)
                    textView.SetTypeface(Typeface.SansSerif, TypefaceStyle.Bold);
                else
                {
                    textView.SetTypeface(Typeface.SansSerif, TypefaceStyle.Normal);
                }
            };

            RadioButton radioButton1 = FindViewById<RadioButton>(Resource.Id.RadiosansID);
            radioButton1.Click += (o, e) => {
                if (radioButton1.Checked)
                    textView.SetTypeface(Typeface.SansSerif, TypefaceStyle.Normal);
                else
                {
                    textView.SetTypeface(Typeface.Serif, TypefaceStyle.Normal);
                }
            };

            RadioButton radioButton2 = FindViewById<RadioButton>(Resource.Id.RadioserifID);
            radioButton2.Click += (o, e) => {
                if (radioButton2.Checked)
                    textView.SetTypeface(Typeface.Serif, TypefaceStyle.Normal);
                else
                {
                    textView.SetTypeface(Typeface.SansSerif, TypefaceStyle.Normal);
                }
            };

            Button offButton = FindViewById<Button>(Resource.Id.offButtonID);
            offButton.Click += (o, e) =>
            {
                Finish();
            };



            }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View)sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}

